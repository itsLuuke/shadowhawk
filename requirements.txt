# https://github.com/ssut/py-googletrans/issues/234#issuecomment-736314530
git+https://github.com/justasic/py-googletrans@feature/enhance-use-of-direct-api

pyrogram>=1.2.20
tgcrypto
requests
aiohttp>=3.7.3
PyYAML
forex-python
ormar[postgresql]
arrow
pillow
sqlalchemy>=1.4.0b3
humanize
dateparser
pint
git+https://github.com/intellivoid/Python-Spam-Protection-API
pyee
Levenshtein
rich
SibylSystem
#nanoid
fastecdsa