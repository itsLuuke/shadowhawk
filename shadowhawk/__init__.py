
import asyncio
import yaml
import aiohttp
import logging
from rich.logging import RichHandler
from pathlib import Path
from pyee import AsyncIOEventEmitter

from pyrogram.parser import parser

__version__ = "1.0.0"

logging.basicConfig(
	format="[%(asctime)s] In thread %(threadName)s, module %(module)s at %(funcName)s line %(lineno)s -> %(message)s",
	level=logging.INFO,
	handlers=[RichHandler()],
)

# Dirty hack to get around python's global variable scope copying
# bulllshit. Thanks @TheKneesocks for this code!
class ObjectProxy:
	def __init__(self, thing=None):
		self.thing = thing

	def __getattr__(self, attr):
		return getattr(self.thing, attr)

	def __bool__(self):
		return bool(self.thing)

	def __repr__(self):
		return repr(self.thing)

	def __str__(self):
		return str(self.thing)

	def __iter__(self):
		yield iter(self.thing)

	def __call__(self, *args, **kwargs):
		return self.thing(*args, **kwargs)

	def get_thing(self):
		return self.thing

	def set_thing(self, thing):
		self.thing = thing

with open("config.yaml") as c:
	config = yaml.safe_load(c)

# Globals.
loop = asyncio.get_event_loop()
apps = dict()
app_user_ids = dict()
log_peer_ids = []
statistics = dict()
slave = ObjectProxy()
plmgr = ObjectProxy()

# Event system
ee = AsyncIOEventEmitter(loop=loop)

# Get the info on what the server supports
server_support = ObjectProxy()

# Http client session for making various HTTP requests
session = aiohttp.ClientSession()

# awaited task load measurements
loads = {
	1: 0.0,  # 1 minute load
	5: 0.0,  # 5 minute load
	15: 0.0,  # 15 minute load
	30: 0.0,  # 30 minute load
}

# this code here exists because i can't be fucked
class Parser(parser.Parser):
	async def parse(self, text, mode):
		if mode == "through":
			return text
		return await super().parse(text, mode)

#################
# ShadowHawk main
#

sessions_path = Path("sessions")
sessions_test_path = sessions_path / "test"
sessions_path.mkdir(exist_ok=True)

# This is here to avoid circular includes.
from shadowhawk.utils.AdminCacheManager import AdminCacheManager
acm = AdminCacheManager()

###############
