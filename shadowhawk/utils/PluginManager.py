import typing
import traceback
import logging
import sys
import gc
import ast
import base64
from io import BytesIO
from pyrogram import Client
from pyrogram.raw.core.primitives import Bytes
from pathlib import Path
from importlib import import_module
from fastecdsa.point import Point
from fastecdsa import ecdsa
from shadowhawk import config
# from pyrogram.handlers.handler import Handler

log = logging.getLogger(__name__)
triangulum = Point(35441751398416857496536618121866780581669428236947586928501161362012395270007,
                   111785166523856556894449646663410979586544367077887196612405512988573023988531)

class Plugin:
	# Name of the plugin
	name = str;
	# What handlers this plugin has
	handlers = dict()
	# The module itself (used for iterating, loading, unloading, etc.)
	module = None
	# The path where the module was loaded from
	module_path = typing.Union[None, Path]
	# Clients which this plugin is registered with
	clients = typing.List[Client]
	# Plugin Cryptographic Signature
	signature = typing.Union[None, str]

	def __init__(self, name, module, module_path, sig=None) -> None:
		self.name = name
		self.module = module
		self.module_path = module_path
		self.handlers = dict()
		self.clients = list()
		self.signature = sig
		for name in vars(self.module).keys():
			try:
				for handler, group in getattr(self.module, name).handlers:
					self.handlers[(name, group)] = (handler, group)
			except Exception:
				pass

	def AddClient(self, client: Client) -> None:
		self.clients.append(client)
		for handler, group in self.handlers.values():
			client.add_handler(handler, group)

	def RemoveClient(self, client: Client) -> None:
		self.clients.remove(client)
		for handler, group in self.handlers.values():
			client.remove_handler(handler, group)

	def GetHelp(self) -> str:
		"""Get the formatted help for this plugin
		"""
		retstr = ""
		for name, _ in self.handlers.keys():
			try:
				retstr += getattr(self.module, name).__doc__ + "\n"
			except Exception:
				pass

		return retstr

	def GetHelpSection(self) -> str:
		"""Get the help "section" for this plugin
		"""
		sect = getattr(self.module, '__help_section__', None)
		return "Miscellaneous" if not sect else sect

class PluginManager:
	plugins = []

	def __init__(self, loaddir: Path) -> None:
		for path in sorted(Path(loaddir.replace(".", "/")).rglob("*.py")):
			self.LoadPlugin(path)

		log.info(f"[LOAD] Loaded a total of {len(self.plugins)} plugins!")

	def VerifyPlugin(self, modulepath: Path) -> typing.Union[None, str]:
		"""Cryptographically verify a plugin
		"""
		shsig = None
		with open(modulepath) as pyfile:
			tree = ast.parse(pyfile.read())
			for node in tree.body:
				if isinstance(node, ast.Assign) and isinstance(node.value, ast.Constant):
					for target in node.targets:
						if target.id == "__signature__":
							shsig = node.value.value
							tree.body.remove(node)
			if not shsig:
				return None

			data = ast.unparse(tree)
			shdat = BytesIO(base64.b64decode(shsig[6:]))
			r = int.from_bytes(Bytes.read(shdat), "big")
			s = int.from_bytes(Bytes.read(shdat), "big")
			if ecdsa.verify((r, s), data, triangulum):
				return shsig
		return None

	def LoadPlugin(self, path: Path) -> typing.Union[None, Plugin]:
		"""Load a plugin into ShadowHawk
		"""
		signature = self.VerifyPlugin(path)
		if not signature:
			if "pluginvalidation" in config['config'] and config['config']['pluginvalidation']['enabled']:
				log.warning(f"[LOAD] [VERIFICATION] Module {path} is not a signed plugin, refusing to load!")
				return
			else:
				log.warning(f"[LOAD] [VERIFICATION] Module {path} is not a signed plugin and plugin verification is disabled, please ensure you trust this plugin!")

		module_path = '.'.join(path.parent.parts + (path.stem,))
		try:
			module = import_module(module_path)

			if "__path__" in dir(module):
				log.warning(f"[LOAD] Ignoring namespace \"{module_path}\"")
				return

			plugin = Plugin(path.stem, module, module_path, signature)
			self.plugins.append(plugin)

			log.info(f"[LOAD]{' [VERIFIED]' if signature else ''} Module {plugin.module_path} loaded with {len(plugin.handlers)} handlers: " + ', '.join(name for name, _ in plugin.handlers))
			return plugin
		except ImportError as ex:
			print(ex)
			log.warning(f"[LOAD] Ignoring non-existant module \"{module_path}\"")
		except Exception:
			traceback.print_exc()
			log.error(f"[LOAD] Failed to load {module_path}")
		return None

	def UnloadPlugin(self, plugin: Plugin) -> None:
		for client in plugin.clients:
			plugin.RemoveClient(client)
		
		self.plugins.remove(plugin)
		
		# XXX: It's not all that easy to remove a module which
		# has already been imported into Python. This may or
		# may not unload said plugin/module
		del sys.modules[plugin.module_path]
		plugin.module = None
		gc.collect()

	def RegisterClient(self, client: Client) -> None:
		log.info(f"[LOAD] Registering new client: {client.session_name}")
		for plug in self.plugins:
			plug.AddClient(client)
		log.info(f"[LOAD] Registration complete for {client.session_name}")

	def UnregisterClient(self, client: Client) -> None:
		log.info(f"[UNLOAD] Removing client: {client.session_name}")
		for plug in self.plugins:
			plug.RemoveClient(client)
		log.info(f"[LOAD] Removal for {client.session_name}")