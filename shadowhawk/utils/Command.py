import re
from pyrogram import Client
from pyrogram.types import Message
from pyrogram.errors.exceptions.bad_request_400 import UsernameNotOccupied
from shadowhawk.utils import get_entity
from shadowhawk import config

# TODO: This needs to support parsing lists in some way
REGEX = re.compile(
	r'--?(?P<flag>\w+)(?:\s*(?:=\s*|)(?:"(?P<string>(?:(?!(?<!\\)").)*)"|(?P<list>\[[^\[\]]+\])|(?P<arg>[\w\@\|\:]+))|)'
)

def parse_command(message: str):
	owo = {}

	# Remove the prefix
	for prefix in config["config"]["prefixes"]:
		if message.startswith(prefix):
			message = message[len(prefix) :]

	# Parse the command structure
	for match in REGEX.finditer(message):
		flag = match.group("flag")
		string = match.group("string")
		arg = match.group("arg")
		items = match.group("list")
		if string:
			owo[flag] = string.replace('\\"', '"')
		elif items:
			# Parsing lists is a bit more complex
			# Sample input: [+CAN_ADD_MEMBERS, +CAN_USE_ANONYMOUS]
			owo[flag] = [x.strip() for x in items[1:-1].split(",")]
		else:
			owo[flag] = arg
	return owo

async def ResolveChatUser(owo: dict, client: Client, message: Message):
	# Attempt to resolve from the following:
	# - Mentions
	# - Replies
	# - Provided arguments

	user = None
	chat = message.chat

	# find keys, if this were C/C++ we could use if-assignment >:(
	key_chat = next((key for key, val in owo.items() if key in ["c", "chat", "group", "g", "channel"]), None)
	key_user = next((key for key, val in owo.items() if key in ["u", "user"]), None)

	# if it's a reply.
	if message.reply_to_message:
		# if it's a channel
		if message.reply_to_message.sender_chat:
			user = message.reply_to_message.sender_chat
		elif message.reply_to_message.from_user:
			user = message.reply_to_message.from_user

	# if there's other entities here to process
	if message.entities:
		for entity in message.entities:
			if entity.type == "text_mention":
				user = entity.user
			elif entity.type == "mention":
				uwu = message.text[entity.offset : entity.offset + entity.length]
				try:
					ent, _ = await get_entity(client, uwu)
					user = ent
				except UsernameNotOccupied:
					# TODO: handle this maybe?
					pass
	if key_user:
		try:
			ent, _ = await get_entity(client, owo[key_user])
			user = ent
		except UsernameNotOccupied:
			# TODO: handle this maybe?
			pass
	if key_chat:
		try:
			ent, _ = await get_entity(client, owo[key_chat])
			chat = ent
		except UsernameNotOccupied:
			# TODO: handle this maybe?
			pass

	return user, chat
