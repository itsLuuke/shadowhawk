import os
import signal
from pyrogram import Client, filters
from shadowhawk import config
from shadowhawk.utils.Logging import log_errors, public_log_errors

__help_section__ = "Poweroff"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.edited
	& ~filters.forwarded
	& filters.me
	& filters.command(
		["poweroff", "shutdown", "stop"], prefixes=config["config"]["prefixes"]
	)
)
@log_errors
@public_log_errors
async def poweroff(client, message):
	"""{prefix}poweroff - Turns off the userbot
Aliases: {prefix}shutdown, {prefix}stop
	"""
	await message.reply_text("Goodbye")
	os.kill(os.getpid(), signal.SIGINT)

__signature__ = "SHSIG-ICEHi1d9cNUl/Go/lVmjM4b3vR4h1iVHT00rWTqE14w7AAAAIARYImoY/mPSUGMsiHtonCVHaRolF2nfBo2VdFhNln+oAAAA"